# On administrator account:

````
sudo apt update -y
sudo apt upgrade -y
sudo add-apt-repository ppa:avsm/ppa
sudo apt -y update
sudo apt install -y opam m4 pkg-config libgtksourceview2.0-dev
````

# On invite account:

````
opam  init
eval $(opam env) 
opam update
opam install dune lablgtk lablgtk-extras
````

And in Laby source directory:

````
dune build --profile release
dune install
````

# Run Laby (on invite account):

````
source ~/profile
laby
````

__WARNING:__ Do not remove the Laby installed from apt! If you do so, an expected conf folder will be missing.

