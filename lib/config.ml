let project_name = "laby"
let version_string = "0.6.4"
let version_base = "27ecf893d093ad88d92ac84fbfc40671cdee0dec"
let version_current = "7e6da5f58ed8760f5572160ae9491851a0bc725a"
let version_status = " .merlin     |  5 +++++
 build       |  2 +-
 data/texts  |  5 +++++
 src/game.ml |  1 -
 src/gfx.ml  | 54 +++++++++++++++++++++++++++++++-----------------------
 5 files changed, 42 insertions(+), 25 deletions(-)"

let build_system = "linux"
let build_arch = "amd64"
let build_ocaml = "4.08.1"
let build_lablgtk = "2.18.8"
let _ = Res.sys_data_dir := "/usr/share"
let _ = Res.sys_tmp_dir := "/tmp"
